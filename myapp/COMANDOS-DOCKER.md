# **COMANDOS-DOCKER:**

* **Construir a imagem:** 

docker build -t myapp:v1 .

* **Criar o container:** 

docker run -d -it --name myapp myapp:v1 ash

* **Entrar/Executar o nosso container:** 

docker exec -it myapp ash

ls

cd /opt/

ls

cd myapp/

ls

cat cv-equipa2.txt

* **Para executar o ficheiro cat equipa2.txt automaticamente:**

docker run -it --name myapp myapp:v1


